import React from "react"
import styled from 'styled-components';
import {  Link } from "gatsby"
import Arrow from "../assets/arrow.svg";

const SectionWrapper = styled.div`  



width:100%:
@media (min-width: 1920px) {
  width: 1920px; }

margin:0 auto;
  }
`;
const ContentWrapper = styled.div`  
width:400px;
margin:0 auto;
@media (min-width: 850px) {
  width:800px; }

@media (min-width: 1140px) {
  width:1140px; }

  }


`;
const PostWrapper = styled.div`  
 display:flex;
 flex-wrap: wrap;&:first-child > a{
    
    display:none;
  
}
 justify-content: space-between;
 @media (max-width: 850px) {
  margin:0 auto; }

  

  }
`;
const SectionTitle = styled.h2`
width: 504px;
text-shadow: 1px 1px 3px #aaaaaa;
color: #ffffff;
font-family: 'Raleway', sans-serif;
font-size: 72px;
font-weight: 800;
line-height: 60px;
margin-left:-3px;

text-transform: uppercase;
margin-bottom:150px;

@media (max-width: 750px) {
  font-size: 40px;
  width: 300px;
  margin-left:20px;
}

div{
  width:62px;
  height:0px;
  border: solid 2px #fdc300;
  margin-bottom:17px;
  margin-left:6px;
}
`;
const PostBox = styled.div`  
background:#dededd;
width:360px;
@media (max-width: 1140px) {
  margin-top:30px; }
  }
  @media (max-width: 850px) {
    margin:0 auto; 
    margin-top:30px; }
`;
const BoxContent = styled.div`  
background:white;
padding-left:28px;
height:184px;

  }
`;
const BoxImg = styled.div`  
background:#dededd;
height:216px;
position: relative;
}
`;
const SectionMainButton = styled.div`  
  width:216px;
  border-radius: 25px;
  background-color: #fdc300;
  margin:0 auto;
  margin-bottom:52px;
  margin-top:50px;
text-align:center;
    a{
      display:block;
      text-decoration: none;
      padding-top:18px;
      padding-left:63px;
      padding-right:63px;
      padding-bottom:16px; 
      color: #ffffff;
      font-family: 'Raleway', sans-serif;
      font-size: 13px;
      font-weight: 700;  
      text-transform: uppercase;
    }
  }
`;
const SectionButton = styled.div`  

width: 165px;
background-color: #1b2936;
position: absolute; 
bottom: 0;
right:0;
text-align:center;
&:hover{
  background-color: #006db7;
  span{
     
     
     background-color: #037ed1;
    
    
   }
}
    a{
      padding-top:7px;
      padding-left:30px;
      padding-bottom:6px;      
      display:block;
      text-decoration: none;
      color: #ffffff;
      font-family: 'Raleway', sans-serif;
      font-size: 13px;
      font-weight: 700;
      line-height: 20px;
      text-transform: uppercase;
      
    }
    span{
     
      margin-left:29px;
      background-color: #2f3840;
      padding:13px 11px 11px 13px;
      color: #ffffff;
      font-size: 9px;
      font-weight: 400;
      line-height: 20px;
    }

    

  }
`;

const PostSubtitle = styled.div` 
width: 303px;
height: 70px;
color: #777777;
font-family: 'Open Sans', sans-serif;
font-size: 13px;
font-weight: 400;
line-height: 20px;
  
  }
`;
const PostTitle = styled.div` 
color: #333333;
font-family: 'Raleway', sans-serif;
font-size: 15px;
font-weight: 800;
text-transform: uppercase;
padding-top:32px;
margin-left:1px;

margin-bottom: 14px;
div{
  margin-bottom: 5px;
  width:27px;
  height:0px;  
  border: solid 1.5px #fdc300;
 

} 
  
  }
`;


class SectionOne extends React.Component {
  render() {
  
    return (
      <SectionWrapper>
        <ContentWrapper>
     

      <SectionTitle><div />{this.props.title}</SectionTitle>
        <PostWrapper>
        
        {this.props.things.map(({ node }) => {
          let title = node.title 
          return (
            <PostBox key={node.title}>
                <BoxImg>
                <SectionButton><Link to="#">READ MORE<span><img src={Arrow} alt="arrow"/></span></Link></SectionButton>
                </BoxImg>
                <BoxContent>
                <PostTitle>
                  <div /><h3>
                {title}
                </h3>  </PostTitle> 
              
                <PostSubtitle><p>{node.subtitle}</p></PostSubtitle>
                </BoxContent>
          </PostBox>
          )
        })}
       
      </PostWrapper>
      <SectionMainButton><Link to="#">View Details</Link></SectionMainButton>
      </ContentWrapper>
      </SectionWrapper>
    )
  }
}

export default SectionOne
