import React from "react"
import styled from 'styled-components';



const SectionWrapper = styled.div`  
background:white;
margin: 0 auto;
margin:0 auto;
width:400px;
@media (min-width: 750px) {
  width:750px; 
  padding-bottom:43px;}

  }

  @media (min-width: 1140px) {
    width: 100%;  }


  }
`;

const ContentWrapper = styled.div`  
margin: 0 auto;
margin:0 auto;
width:400px;
@media (min-width: 750px) {
  width:750px; }

  }

  @media (min-width: 1140px) {
    width: 1140px; }


  }
`;
const MainSectionWrapper = styled.div`  
display:flex;
flex-wrap: wrap;
justify-content: space-between;
margin: 0 auto;
@media (max-width: 1140px) {

justify-content: space-evenly;
}
  }
`;
const TitleSection = styled.h2`  
text-align:center;
  color: #333333;
  font-family: 'Raleway', sans-serif;
  font-size: 30px;
  font-weight: 700;
  padding-top: 69px;
  line-height: 20px;
  
  }
`;

const Icon = styled.img`  
margin-left:25px;
  }
`;
const SubtitleSection = styled.h3`  
color: #888888;
font-family: 'Raleway', sans-serif;
font-size: 13px;


text-transform: uppercase;
text-align:center;
margin-top:12px;




div{
    width:27px;
    height:0px;
    border: solid 1.5px  #fdc300;;
    margin:0 auto;
    margin-top:11.5px;
    margin-bottom: 37px;
}
  }
`;
const OfferElement = styled.div`  
display:flex;
flex-wrap: wrap;
width:350px;
background-color: #fcfcfc;



@media (max-width: 1139px) {
  justify-content: center;
  text-align:center;
  margin:0 auto;
}
@media (min-width: 1140px) {
  width:555px;
  height:182px;
  margin-bottom:30px;
  align-items: center;
  justify-content:  center;

}
  }}
`;

const OfferCard = styled.div`  
margin-bottom:8px;
}`;


const ShortText = styled.p`  

color: #777777;
font-family: 'Open Sans', sans-serif;
font-size: 13px;
font-weight: 400;
line-height: 20px;
height: 73px;
width:360px;

word-wrap: break-word;
margin-left: 32px;
  }
`;
const OfferTitle = styled.h3`  

color: #333333;
font-family: 'Raleway', sans-serif;
font-size: 18px;
font-weight: 800;
margin-bottom:20px;
margin-left: 32px;


  }
`;



class SectionSecond extends React.Component {
  render() {
  
    return (
      <SectionWrapper>
        <ContentWrapper>
      <TitleSection>{this.props.title}</TitleSection>
      <SubtitleSection>{this.props.subtitle}<div/></SubtitleSection>
        <MainSectionWrapper>        
        {this.props.things.map(({ node }) => {
          let title = node.title 
          return (
            <OfferElement key={node.title}>
              <Icon  src={node.icon.resolutions.src} alt="icon"/>
                <OfferCard><OfferTitle>{title}</OfferTitle>
                <ShortText>{node.shorttext}</ShortText>
                </OfferCard>
            </OfferElement>
          )
        })}
       
      </MainSectionWrapper>
      </ContentWrapper>
      </SectionWrapper>
    )
  }
}

export default SectionSecond
