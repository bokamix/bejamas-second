import React from "react"
import {  Link } from "gatsby"
import styled from 'styled-components'
import LogoNav from '../assets/logo.png'
import SaerchIcon from '../assets/saerch.png'


const MenuWrapper = styled.div`  
display:flex;
flex-wrap: nowrap;
justify-content: flex-start;  

width: 1140px;
height:81px;
margin: 0 auto;
margin-bottom: 164px;
box-shadow: 0 -8px 0 rgba(255, 255, 255, 0.2), inset 1px 1px 0 rgba(201, 202, 202, 0.75);
  border-radius: 3px;
  background-color: #384653;
  background-image: linear-gradient(to top, #fcfcfc 0%, #ffffff 100%);
  @media (max-width: 1140px) {
    display:none; }

  }
`;
const NavWrapper = styled.ul`  
display:flex; 
list-style-type: none;
padding-inline-start: 0;
margin-top: 35px;
  }
`;
const NavItem = styled.li` 
margin-right:46px;

a{
text-decoration: none;
display:block;
color: #333333;
font-family: 'Raleway', sans-serif;
font-size: 13px;
font-weight: 800;

text-transform: uppercase;

}
a:hover{
  text-decoration: none;
display:block;
color: #333333;
font-family: 'Raleway', sans-serif;
font-size: 13px;
font-weight: 700;
position: relative; 
text-transform: uppercase;

 div{
  border-bottom:5px solid #006db7;
  height: 0px;  
  width:66px;
  margin-top:26px;  
  margin-left: -31px;
  left:50%;



  position: absolute; 
 
 }
}

a:active{
  text-decoration: none;
display:block;
color: #333333;
font-family: 'Raleway', sans-serif;
font-size: 13px;
font-weight: 700;

text-transform: uppercase;
}
a:visited{
  text-decoration: none;
display:block;
color: #333333;
font-family: 'Raleway', sans-serif;
font-size: 13px;
font-weight: 700;

text-transform: uppercase;
}
  }
`;

const Logo = styled.img` 
margin-right:281px;
margin-left:23px;
margin-top: 19px;
  }
`;
const SearchIcon = styled.img` 

width:35px;
height:35px;
margin-left: -14px;
    margin-top: -11px;
  }
`;


class Navigation extends React.Component {
  render() {
  
    return (
      
        <MenuWrapper>
          <div><Logo src={LogoNav} /></div>
    <nav className="menu">
  <NavWrapper>
    <NavItem><Link to="#">Home<div /></Link></NavItem>
    <NavItem><Link to="#">About Us<div /></Link></NavItem>
    <NavItem><Link to="#">Services<div /></Link></NavItem>
    <NavItem><Link to="#">News<div /></Link></NavItem>
    <NavItem><Link to="#">Locations<div /></Link></NavItem>
    <NavItem><Link to="#">Contact<div /></Link></NavItem>
    <SearchIcon src={SaerchIcon}/>
  </NavWrapper>
</nav>
       
      </MenuWrapper>
    )
  }
}

export default Navigation
